<?php
/**
 * 
 */
class loginmodel extends CI_Model
{
	public function login_validation($username,$password)
	{
		$login_query = $this->db->where(['username'=>$username,'password'=>$password])
								->get('users');
		if($login_query->num_rows())
		{
			return $login_query->row()->id;
		}
		else
		{
			return false;
		}
	}
	public function articlelist($limit,$offset)
	{
		$id = $this->session->userdata('id');
		$q=$this->db->select()
					->from('articles')
					->where(['user_id'=>$id])
					->limit($limit,$offset)
					->get();
		return $q->result();
	}
	public function num_rows()
	{
		$id = $this->session->userdata('id');
		$q=$this->db->select()
					->from('articles')
					->where(['user_id'=>$id])
					->get();
		return $q->num_rows();
	}
	public function add_article($article)
	{
		return $this->db->insert('articles',$article);
	}
	public function add_user($user)
	{
		 return $this->db->insert('users',$user);
	}
	
	public function del_article($id)
	{
		return $this->db->delete('articles',['id'=>$id]);
	}
	public function find_article($article_id)
	{
		$q = $this->db->select()
				 		->where('id',$article_id)
				 		->get('articles');
		return $q->row();
	}
	public function update_article($article_id,$article)
	{
		return $this->db->where('id',$article_id)
				 ->update('articles',$article);
	}
	public function all_article($limit,$offset)
	{
		# code...
		$q = $this->db->select()
				 ->from('articles')
				 ->limit($limit,$offset)
				 ->get();
		return $q->result();
	}
	public function all_num_rows()
	{
		$q=$this->db->select()
					->from('articles')
					->get();
		// print_r($q->num_rows());
		return $q->num_rows();

	}
	public function article_detail($id)
	{
		$q=$this->db->where('id',$id)
					->get('articles');
		return $q->row();
	}
}