<?php include 'header.php'; ?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style type="text/css">
		.pagination {
  display: inline-block;
  padding-left: 0;
  margin: 21px 0;
  border-radius: 0;
}
.pagination > li {
  display: inline;
}
.pagination > li > a,
.pagination > li > span {
  position: relative;
  float: left;
  padding: 8px 12px;
  line-height: 1.4;
  text-decoration: none;
  color: #008cba;
  background-color: transparent;
  border: 1px solid transparent;
  margin-left: -1px;
}
.pagination > li:first-child > a,
.pagination > li:first-child > span {
  margin-left: 0;
  border-bottom-left-radius: 0;
  border-top-left-radius: 0;
}
.pagination > li:last-child > a,
.pagination > li:last-child > span {
  border-bottom-right-radius: 0;
  border-top-right-radius: 0;
}
.pagination > li > a:hover,
.pagination > li > span:hover,
.pagination > li > a:focus,
.pagination > li > span:focus {
  z-index: 2;
  color: #008cba;
  background-color: #eeeeee;
  border-color: transparent;
}
.pagination > .active > a,
.pagination > .active > span,
.pagination > .active > a:hover,
.pagination > .active > span:hover,
.pagination > .active > a:focus,
.pagination > .active > span:focus {
  z-index: 3;
  color: #ffffff;
  background-color: #008cba;
  border-color: transparent;
  cursor: default;
}
.pagination > .disabled > span,
.pagination > .disabled > span:hover,
.pagination > .disabled > span:focus,
.pagination > .disabled > a,
.pagination > .disabled > a:hover,
.pagination > .disabled > a:focus {
  color: #999999;
  background-color: #ffffff;
  border-color: transparent;
  cursor: not-allowed;
}
	</style>
</head>
<body>
	<div class="container" style="margin-top: 30px">
		<div class="row">
			<?= anchor('admin/add_article','Add Article',['class'=>'btn btn-info btn-lg']) ?>
		</div>
	</div>
	<div class="container" style="margin-top: 30px">
	<?= heading('* Article List *','1',['style'=>'font-weight:500; font-style:italic']); ?>
	<?php if($error=$this->session->flashdata('msg')): ?>
<div>
	<div class="row">
		<div class="col-md-5 <?= $this->session->flashdata('msg_alert') ?>" style="border-radius: 0.5rem;font-size: 16px; font-weight: 500;">
		<?= $error; ?>
		</div>
	</div>
</div>
<?php endif; ?>
	<table class="table col-md-7" style="margin-top: 20px">
		<thead>
			<th>Sr.No</th>
			<th>Artilce Title</th>
			<!-- <th>Article Body</th> -->
			<th>Edit</th>
			<th>Delete</th>
		</thead>
		<tbody>
		<?php if(count($article)): $i = $this->uri->segment(3);  foreach ($article as $art) : $i++;
				?>
				<tr>
					<th><b><?php if($i>10){echo $i;}else{echo "0".$i;}?></b></th>
				<td><?= $art->article_title ?></td>
				<!-- <td><?= $art->article_body ?></td> -->
				<!-- <td><a href="" class="btn btn-primary" style="width: 60px">Edit</a></td> -->
				<td><?= anchor("admin/edit_article/$art->id",'Edit',['class'=>'btn btn-primary','style'=>'width: 60px']) ?></td>
				<td>
					<?= form_open_multipart('admin/del_article'); ?>
					<?= form_hidden('id',$art->id) ?>
					<?= form_submit('','Delete',['class'=>'btn btn-danger']); ?>
					<?= form_close() ;?>

				</td>
			</tr>
		<?php endforeach;?>
		<?php else: ?>
			<tr>
				<td colspan="4"><h2 style="font-weight: 500">Sorry... No Data Available</h2></td>
			</tr>
			<?php
	endif;   ?>
		</tbody>
	</table>
	<?= $this->pagination->create_links();?>
	</div>
</body>
</html>
<?php include 'footer.php' ?>