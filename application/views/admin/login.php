<?php include('header.php'); ?>
<!DOCTYPE html>
<html>
<head>
	
</head>
<body>
	<div class="container">
<div style="margin-top: 40px">
	<label style="font-size:50px; font-style: italic; font-weight: 500; text-align: center;">Admin Login</label>
</div>
<?php if($this->session->flashdata('add_user')){ ?>
<div class="row">
	<div class="col-md-5">
	<div class="<?= $this->session->flashdata('add_user_alert'); ?>">
		<?= $this->session->flashdata('add_user'); ?>
	</div>
	</div>
</div>
<?php } ?>
<div>
	<div class="row">
		<div class="col-md-5 <?= $this->session->flashdata('msg_alert'); ?>" style="border-radius: 0.5rem;font-size: 16px; font-weight: 500;">
		<?= $this->session->flashdata('msg');; ?>
		</div>
	</div>
</div>
<div>
<?= form_open_multipart('login/loginvalidate') ?>
<div class="row">
	<div class="col-md-5">
		<label class="form-label">Enter your username :</label>
		<?= form_input(['class'=>'form-control','placeholder'=>'Username','name'=>'username','value'=>set_value('username')]).br() ?>
	</div>
	<div class="col-md-7" style="padding-top: 25px">
		
		<?= form_error('username'); ?>
	</div>
</div>
<div class="row">
	<div class="col-md-5">
		<label class="form-label">Enter your password :</label>
		<?= form_password(['class'=>'form-control','placeholder'=>'Password','name'=>'password','value'=>set_value('password')]).br()?>
	</div>
	<div class="col-md-7" style="padding-top: 25px">
		<?= form_error('password'); ?>
	</div>
</div>
	<div class="row">
		<div class="col-md-5">
			<?= form_submit(['type'=>'submit','class'=>'btn','value'=>'LogIn','style'=>'width:100px']).nbs(2); ?>

			<?= form_submit(['type'=>'reset','class'=>'btn btn-primary','value'=>'Reset','style'=>'width:100px']) ?>
			<?= anchor('login/register','Sign Up',array('style'=>'padding-left:15px','class'=>'link')); ?>
		</div>
	</div>
</div></div>
</body>
</html>
<?php include('footer.php');?>