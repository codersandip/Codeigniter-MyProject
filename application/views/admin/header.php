<?= link_tag(base_url('Assets/bootstrap.min.css')); ?>
<?= link_tag(base_url('Assets/cs.css')); ?>
<script src="<?= base_url('Assets/bootstrap.js'); ?>"></script>
<script src="<?= base_url('Assets/jquery.min.js'); ?>"></script>
<link rel="icon" type="png" href="<?= base_url('images/project/sp.png');?>">
<nav class="navbar navbar-expand-lg navbar-dark bg-primary sticky-top">
  <a class="navbar-brand" href="" style="font-weight: 450">My Project</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarColor01">
    <ul class="navbar-nav mr-auto">
    <?php if($this->session->userdata('id')){ ?>
      <li class="nav-item">
        <?= anchor('admin/logout','Logout',['class'=>' btn btn-danger','style'=>'margin-left: 10px']); ?>
      </li>
    <?php } ?>
    <?php if(!$this->session->userdata('id')){ ?>
      <li class="nav-item">
        <?= anchor('users','User Panel',['class'=>'nav-link','style'=>'margin-left: 10px;font-weight:550; font-size:17px']); ?>
      </li>
    <?php } ?>
    </ul>
  </div>
</nav>

