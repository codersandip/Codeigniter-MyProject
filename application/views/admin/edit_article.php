<?php include 'header.php'; $user_id = $this->session->userdata('id'); ?>
<div class="container" style="margin-top: 30px">
	<?= heading('*Edit Article*','1',['style'=>'font-weight:500; font-style:italic']); ?>
<?= form_open_multipart("admin/update_article/{$article->id}") ?>
<div class="row">
	<div class="col-md-5">
		<label class="form-label" style="font-size: 16px; font-weight: 400">Article Title :</label>
		<?= form_input(['class'=>'form-control','placeholder'=>'Enter Article Title','name'=>'article_title','value'=>set_value('article_title',$article->article_title)]).br();  ?>
	</div>
	<div class="col-md-7 text-danger" style="padding-top: 25px">
		
		<?= form_error('article_title'); ?>
	</div>
</div>
<div class="row">
	<div class="col-md-5">
		<label class="form-label" style="font-size: 16px; font-weight: 400">Article Body :</label>
		<?= form_textarea(['class'=>'form-control','placeholder'=>'Article Body','name'=>'article_body','value'=>set_value('article_body',$article->article_body)]).br()?>
	</div>
	<div class="col-md-7 text-danger" style="padding-top: 25px">
		<?= form_error('article_body'); ?>
	</div>
</div>
<!-- <?= form_input(['value'=>$user_id,'name'=>'user_id','hidden'=>'']); ?> -->
	<div class="row">
		<div class="col-md-5">
			<?= form_submit(['type'=>'submit','class'=>'btn','value'=>'Update Article','style'=>'']).nbs(2); ?>

			<?= form_submit(['type'=>'reset','class'=>'btn btn-primary','value'=>'Reset','style'=>'width:70px']) ?>
		</div>
	</div>
</div>
<?php include 'footer.php';