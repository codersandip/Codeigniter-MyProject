<?php include 'header.php';
?>
<div class="container">
	<div style="margin-top: 50px">
		<h1>Register Form</h1>
	</div>
	<?= form_open_multipart('login/register_validate',['class'=>'form-group']); ?>
	<div class="row">
		<div class="col-lg-5">
			<?= form_label('Enter Your First Name :','',['style'=>'font-size:16px']).
			form_input(['class'=>'form-control','name'=>'first_name','value'=>set_value('first_name'),'placeholder'=>'Enter Your First Name']).br(); ?>
		</div>
		<div class="col-lg-5" style="padding-top: 30px">
		<?= form_error('first_name')?>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-5">
			<?= form_label('Enter Your Last Name :','',['style'=>'font-size:16px']).
			 form_input(['class'=>'form-control','name'=>'last_name','value'=>set_value('last_name'),'placeholder'=>'Enter Your Last Name']).br();?>
		</div>
		<div class="col-lg-5" style="padding-top: 30px">
		<?= form_error('last_name')?>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-5">
			<?= form_label('Enter Your Username :','',['style'=>'font-size:16px']).
		 form_input(['class'=>'form-control','name'=>'username','value'=>set_value('username'),'placeholder'=>'Enter Your Username']).br(); ?>
		</div>
		<div class="col-lg-5" style="padding-top: 30px">
		<?= form_error('username')?>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-5">
			<?= form_label('Enter Your Password :','',['style'=>'font-size:16px']).
		 form_input(['class'=>'form-control','name'=>'password','value'=>set_value('password'),'placeholder'=>'Enter Your Password']).br(); ?>
		</div>
		<div class="col-lg-5" style="padding-top: 30px">
		<?= form_error('password')?>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-5">
			<?=  form_label('Enter Your Email Id :','',['style'=>'font-size:16px']).
		 form_input(['class'=>'form-control','name'=>'email','value'=>set_value('email'),'placeholder'=>'Enter Your Email Id']).br(); ?>
		</div>
		<div class="col-lg-5" style="padding-top: 30px">
		<?= form_error('email')?>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-5">
			<?= form_submit('','Signup',['class'=>'btn btn-primary','style'=>'width:90px']).form_reset('reset','Cancel',['class'=>'btn btn-danger','style'=>'width:90px; margin-left:15px'])?>
<a href="<?= base_url(); ?>" style="padding-left: 20px; font-size: 17px">Already Have An Account</a>
		</div>
	</div>
<?= form_close() ?>
</div>
<?php include 'footer.php'; 
