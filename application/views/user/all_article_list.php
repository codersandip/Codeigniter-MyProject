<?php include 'header.php'; ?>
<script type="text/javascript">
	$(document).ready(function(){
		$("#myInput").on("onkeyup",function(){
			var value = $(this).val().toLowerCase();
			$("#myTable tr").filter(function(){
				$(this).toggle($(this).text().toLowerCase().indexOf(value) >-1)
		});
	});
});
</script>
<div class="container">
	<form class="" role="search" method="post">
		<div class="form-group col-sm-3">
	    	<input type="search" class="form-control" placeholder="Search" aria-label="Search" id="myInput">
	    </div>
	    <div style="">
		    <button type="submit" class="btn btn-default">Submit</button>
	    </div>
	</form>
	<span style="font-style:italic; font-size: 50px; color: #317eac">Article List</span>
	<table class="table table-striped table-bordered">
		<thead>
			<tr>
				<th>Sr No</th>
				<th style="width: 85">Article Image</th>
				<th>Article Title</th>
				<th>Published On</th>
			</tr>
		</thead>
		<tbody id="myTable">
			<?php if(count($articles)): $i = $this->uri->segment(3);  foreach ($articles as $art) {$i++;
				?>
				<tr>
					<th><?php if($i>10){echo $i;}else{echo "0".$i;}?></th>
					<td><?php if(!is_null($art->image_path)) : ?><img src="<?= $art->image_path ?>" height="200" align=""><?php else: echo 'Sorry Article has no Image'; endif; ?></td>
					<td><!-- <a href="{}"></a> --><?= anchor("users/article/{$art->id}",$art->article_title); ?></td>
					<td><?= date('d M Y h:i:s A',strtotime($art->created_at)); ?></td>
				</tr>
		<?php } endif; ?>
			<tr>
				
			</tr>
		</tbody>
	</table>
	<?= $this->pagination->create_links();?>
</div>
<?php include 'footer.php'; ?>