<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Login extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		if($this->session->userdata('id'))
		{
			return redirect('admin/welcome');
		}
	}
	public function index()
	{
		$this->load->view('admin/login');
	}
	public function loginvalidate()
	{
$this->form_validation->set_rules('username','User Name','required|alpha_numeric|max_length[15]|min_length[1]');
$this->form_validation->set_rules('password','Password','required|alpha_numeric|max_length[15]|min_length[1]');
$this->form_validation->set_error_delimiters('<div class=" text-danger">', '</div>');
if($this->form_validation->run())
{
	$username = $this->input->post('username');
	$password = $this->input->post('password');
	$this->load->model('loginmodel');
	if($this->loginmodel->login_validation($username,$password))
{
	$id = $this->loginmodel->login_validation($username,$password);
	$this->session->set_userdata('id',$id);
$se =	$this->session->userdata('id');
		$this->session->set_flashdata('msg','You are login Successfully');
		$this->session->set_flashdata('msg_alert','alert-succes aler');
 return redirect('admin/welcome');
}
else
{
		$this->session->set_flashdata('msg_alert','alert-dange aler');
	$this->session->set_flashdata('msg','Invalid username/password');
	return redirect('login');
}

}
else
{
	$this->load->view('admin/login');
}

	}
	function register()
	{
		$this->load->view('admin/register');
	}
	function register_validate()
	{
		$this->form_validation->set_rules('first_name','First Name','required|alpha');
		$this->form_validation->set_rules('last_name','Last Name','required|alpha');
		$this->form_validation->set_rules('username','Username','required|alpha_numeric|min_length[3]|max_length[15]|is_unique[users.username]');
		$this->form_validation->set_rules('password','Password','required|alpha_numeric');
		$this->form_validation->set_rules('email','Email','required|valid_email');
		if($this->form_validation->run())
		{
			$array=$this->input->post();
			$this->load->model('loginmodel');
			if($this->loginmodel->add_user($array))
			{
				$this->session->set_flashdata('add_user','Successfully Created User...');
				$this->session->set_flashdata('add_user_alert','aler alert-succes');
				return redirect('login');
			}
			else
			{
				$this->session->set_flashdata('add_user','Failed Created User....!');
				$this->session->set_flashdata('add_user_alert','aler alert-dange');
				return redirect('admin/login');
			}
		}
		else
		{
			$this->form_validation->set_error_delimiters('<div class="text-danger">', '</div>');
			$this->load->view('admin/register');
		}
	}
}?>