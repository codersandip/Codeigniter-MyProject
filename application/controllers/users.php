<?php
/**
 * 
 */
class users extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
			$this->load->model('loginmodel');
		if($this->session->userdata('id'))
		{
			return redirect('admin/welcome');
		}
	}
	public function index()
		{
			 $config=[
			 		'base_url'=>base_url('users/index'),
			 		'per_page'=>2,
			 		'total_rows'=>$this->loginmodel->all_num_rows(),
			 		'suffix' => '.jsp',
			 		'full_tag_open'=>'<ul class="pagination ">',
					'full_tag_close'=>'</ul>',
					'next_tag_open'=>'<li>',
					'next_tag_close'=>'</li>',
					'prev_tag_open'=>'<li>',
					'prev_tag_close'=>'</li>',
					'num_tag_open'=>'<li>',
					'num_tag_close'=>'</li>',
					'cur_tag_open'=>'<li class="active"><a>',
					'cur_tag_close'=>'</a></li>',
			 	];
			$this->pagination->initialize($config);
			$articles = $this->loginmodel->all_article($config['per_page'],$this->uri->segment(3));
			$this->load->view('user/all_article_list',compact('articles'));
		}
		public function article($id)
		{
			$article=$this->loginmodel->article_detail($id);
			$this->load->view('user/article_details',compact('article'));
		}
}