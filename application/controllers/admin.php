<?php
/**
 * 
 */
class admin extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('loginmodel');
		if(!$this->session->userdata('id'))
		{
			return redirect('login/');
		}
	}
	public function welcome()
	{
		$config=[
					'base_url'=>base_url('admin/welcome'),
					'per_page'=>3,
					'total_rows'=>$this->loginmodel->num_rows(),
					// 'suffix' => '.jsp',
			 		'full_tag_open'=>'<ul class="pagination">',
					'full_tag_close'=>'</ul>',
					'next_tag_open'=>'<li>',
					'next_tag_close'=>'</li>',
					'prev_tag_open'=>'<li>',
					'prev_tag_close'=>'</li>',
					'num_tag_open'=>'<li>',
					'num_tag_close'=>'</li>',
					'cur_tag_open'=>'<li class="active"><a>',
					'cur_tag_close'=>'</a></li>',
				];
				$this->pagination->initialize($config);
	$article = $this->loginmodel->articlelist($config['per_page'],$this->uri->segment(3));
		$this->load->view('admin/dashboard',['article'=>$article]);
	}
	public function register()
	{
		$this->load->view('admin/register');
	}
	public function logout()
	{
		$this->session->set_flashdata('msg','You are logout Successfully');
		$this->session->set_flashdata('msg_alert','alert-succes aler');
		$this->session->unset_userdata('id');
		return redirect('login');
	}
	public function add_article()
	{
		$this->load->view('admin/add_article');
	}
	public function insert_article()
	{
		$config=['upload_path' => './images/upload/',
        'allowed_types' => 'gif|jpg|png',
    ];
        $sa=$this->load->library('upload', $config);
		if($this->form_validation->run('add_article_rules') && $this->upload->do_upload('userfile'))
		{
	        $data = $this->upload->data();
			$post = $this->input->post();
        	$image_path = base_url('images/upload/').$data['file_name'];
			$post['image_path'] = $image_path;
			if($this->loginmodel->add_article($post))
			{

				$this->session->set_flashdata('msg','Article Insert Successfully');
				$this->session->set_flashdata('msg_alert','alert-succes aler');
				return redirect('admin/welcome');
			}
			else
			{
				$this->session->set_flashdata('msg','Article Insert Failed');
				$this->session->set_flashdata('msg_alert','alert-denge aler');
				return redirect('admin/welcome');
			}
		}
		else
		{
			$upload_error = $this->upload->display_errors();
			$this->load->view('admin/add_article',compact('upload_error'));
		}      
	}
	public function del_article()
	{
		$post = $this->input->post('id');
		if($this->loginmodel->del_article($post))
			{
				$this->session->set_flashdata('msg','Article Deleted Successfully');
				$this->session->set_flashdata('msg_alert','alert-succes aler');
				return redirect('admin/welcome');
			}
		else
		{
			$this->session->set_flashdata('msg','* Article Deleted Failed');
			$this->session->set_flashdata('msg_alert','alert-denge aler');
			return redirect('admin/welcome');
		}
	}
	public function edit_article($id)
	{
		$article = $this->loginmodel->find_article($id);
		$this->load->view('admin/edit_article',['article'=>$article]);
	}
	public function update_article($article_id)
	{
		if($this->form_validation->run('add_article_rules'))
		{
			$post = $this->input->post();
			if($this->loginmodel->update_article($article_id,$post))	
			{
				$this->session->set_flashdata('msg','Article Updated Successfully');
				$this->session->set_flashdata('msg_alert','alert-succes aler');
				return redirect('admin/welcome');

			}
		}
		else
		{
			$this->edit_article($article_id);
		}
	}
}